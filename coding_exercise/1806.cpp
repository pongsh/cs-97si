//unfinished

#include <iostream>

using namespace std;

#define numScenario fuel*2+1
#define maxRow numScenario
#define maxCol numScenario

void draw(int);

int main() {
    int cases;
    cin >> cases;

    for (int i = 0; i != cases; ++i) {
        int fuel;
        cin >> fuel;
        draw(fuel);        
    }
    return 0;
}

void draw(int fuel) {
    for (int scenario = 0; scenario != numScenario; ++scenario) {
        cout << "Scenario #" << scenario+1 << ":" << endl;
        cout << "slice #" << scenario+1 << ":" << endl;
        for (int row = 0; row != maxRow; ++row) {
            for (int col = 0; col != maxCol; ++ col) {
                if (row+col+scenario <= fuel) {
                    cout << fuel - row*col*scenario << " ";
                }
                else {
                    cout << ". "; 
                }
            }
            cout << endl;
        }
        cout << endl;
    }
}

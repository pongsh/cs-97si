// Hangover
// Input:	the amount hanging over the first card	
// Output:	the minimum number of cards necessary to achieve an overhang of at least c card lengths

#include <iostream>

using namespace std;

int main() {
    float cardLength;
    while(cin >> cardLength) {
        float cards = 0, currentLength = 0;
        if (cardLength == 0) { break; } 
        while (currentLength < cardLength) {
            currentLength += 1/(cards + 2);
            cards += 1.0;
        }            
        cout << cards << " card(s)" << endl;
    }   
    return 0;
}

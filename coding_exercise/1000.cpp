// A+B Problem
// Input:	two integers A and B
// Output:	the sum of A and B

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
	int a, b;
	cin >> a >> b;
	cout << a+b << endl;
	return 0;
}

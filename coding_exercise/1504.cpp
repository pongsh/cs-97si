// Adding reversed numbers
// Input:    two positive integers
// Output:   reverse of the sum of the two integers reversed

#include <iostream>

using namespace std;

int reverse(int);

int main() {
    int cases;
    cin >> cases;

    for (int i = 0; i != cases; ++i) {
        int a, b;
        cin >> a >> b;
        
        int reversedSum = reverse(reverse(a) + reverse(b));
        cout << reversedSum << endl;
    }
    
    return 0;
}

int reverse(int num) {
    int reversedNum = 0;
    while (num > 0) {
        reversedNum = reversedNum * 10 + num % 10;
        num /= 10;
    }
    return reversedNum;
}

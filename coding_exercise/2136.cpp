// Vertical Histogram
// Input:    lines of text
// Output:   histogram showing the number of each letter in the text

#include <iostream>
#include <vector>
#include <algorithm>
#include <cctype>

using namespace std;

int main() {
    string input, line;
    
    while (getline(cin, line)) {
        input += line;
    }

    vector<int> alphabet(26, 0);
    for (int i = 0; i != input.length(); ++i) {
        if (isalpha(toupper(input[i]))) {
            alphabet[input[i] - 'A']++;
        }
    }

    for (int i = *max_element(alphabet.begin(), alphabet.end()); i != 0; --i) {
        for (int j = 0; j != 26; ++j) {
            if (alphabet[j] >= i) {
                cout << "* ";
            }
            else {
                cout << "  ";
            }
        }
        cout << endl;
    }   

    for (char i = 'A'; i <= 'Z'; ++i) {
        cout << i << " ";
    }
    cout << endl;

    return 0;
}

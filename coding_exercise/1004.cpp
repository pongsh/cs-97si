#include <iostream>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;

int main() {
    float balance, count = 0, total = 0;
	while (cin >> balance) {
	    total += balance;
        count += 1.0;
    }
    cout << "$" << fixed << setprecision(2) << total/count << endl;
    return 0;
}	   

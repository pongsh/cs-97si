// Hardwood species
// Input:	list of the species of every tree, one tree per line
// Output:	list of each species and the percentage of the population it represents in alphabetical order

#include <iostream>
#include <map>
#include <iomanip>

using namespace std;

int main() {
	string species;
	map<string,int> trees;
	int numTrees = 0;	

	while (getline(cin, species)) {
		++numTrees;
	
		if (trees.find(species) != trees.end()) {
			++trees[species];
		}
		else {
			trees[species] = 1;
		}
	}

	for (map<string,int>::iterator it = trees.begin(); it != trees.end(); ++it) {
		cout << it-> first << " " << fixed << setprecision(4) << it->second/(float)numTrees*100 << endl;	
	}

	return 0;
}
		
